import React from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import CustomizedInputs from './cadastro';
import './App.css';

function App() {
  return (
    <div className="App">
            <div className="container">
                <Typography variant="h4" gutterBottom>
                    Cadastro:
                </Typography>

                <CustomizedInputs />
                <Button variant="contained" color="primary">
                    <Typography variant="button" display="block" gutterBottom>
                        Salvar
                    </Typography>
                </Button>
            </div>
        </div>
  );
}

export default App;
