import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';

import Typography from '@material-ui/core/Typography';

const currencies = [
    {
        value: 'Pessoa Jurídica',
        label: 'Pessoa Jurídica',
    },
    {
        value: 'Pessoa Física',
        label: 'Pessoa Física',
    },
];

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 200,
        },
    },
    margin: {
        margin: theme.spacing(1),
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },
}));

export default function CustomizedInputs() {
    const classes = useStyles();
    const [currency, setCurrency] = React.useState('BRL');

    const handleChange = event => {
        setCurrency(event.target.value);
    };

    return (
        <>
            <form className={classes.root} noValidate autoComplete="off">
                <TextField
                    id="standard-select-currency"
                    select
                    label="TIPO"
                    value={currency}
                    onChange={handleChange}
                >
                    {currencies.map(option => (
                        <MenuItem key={option.value} value={option.value}>
                            {option.label}
                        </MenuItem>
                    ))}
                </TextField>
                <TextField id="standard-basic" label="CNPJ" defaultValue=" " />
                <TextField
                    id="standard-basic"
                    label="RAZÃO SOCIAL"
                    defaultValue=" "
                />
                <TextField id="standard-basic" label="EMAIL" defaultValue=" " />
            </form>

            <Typography variant="h6" gutterBottom>
                Endereço:
            </Typography>

            <form className={classes.root} noValidate autoComplete="off">
                <TextField
                    id="standard-basic"
                    label="LOGRADOURO"
                    defaultValue=" "
                />
                <TextField
                    id="standard-basic"
                    label="NUMERO"
                    defaultValue=" "
                />
                <TextField
                    id="standard-basic"
                    label="BAIRRO"
                    defaultValue=" "
                />
                <TextField
                    id="standard-basic"
                    label="TELEFONE"
                    defaultValue=" "
                />
            </form>
        </>
    );
}
